function insertaFila(tabla){
    var $tr = $('#'+tabla).find("tbody tr:last").clone();

    var nombreDescarga=$tr.find('.descargaFichero').attr('nombre');
    $tr.find('.descargaFichero').after("<input type='file' name='"+nombreDescarga+"' id='"+nombreDescarga+"' />");

    $tr.find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    }).attr("id", function(){
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find("input[name='filasTabla[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    
    $tr.find("button:not(.selectpicker),.eliminaFila").attr("id", function(){
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find(".botonSelectAjax").attr("id", function(){
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find("input:not([type=checkbox],[type=radio]),textarea").attr("value","");
	$tr.find("input[type=checkbox]").removeAttr("checked");
    $tr.find("select").val("NULL");
    $tr.find('.bootstrap-select').remove();
    $tr.find('.bootstrap-filestyle').remove();
    $tr.find('.descargaFichero').remove();

    $('#'+tabla).find("tbody tr:last").after($tr);
    
    if(typeof jQuery.fn.selectpicker=='function'){
        $tr.find(".selectpicker").selectpicker('refresh');
    }
    if(typeof jQuery.fn.filestyle=='function'){
        $tr.find(':file').filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
    }
    if(typeof jQuery.fn.rating=='function'){
        var campoRating=$tr.find('.rating');
        $tr.find('.rating-input').replaceWith(campoRating);
        campoRating.rating();
    }

    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    if(tabla=='tablaTecnicos'){
        var i=$('#'+tabla).find("tbody tr:last").find('.selectpicker').attr('id').match(/(\D+)(\d*)$/);
        i=i[2]
        $('#codigoTecnico'+i).change(function(){
            oyenteTecnicoRepetido(i);
        });
    }
}

$(document).on('click','.eliminaFila',function(){
  if($('table').find("tbody tr").length>1){
    var fila=$(this).attr('id').match(/(\D+)(\d*)$/);
    fila=fila[2];
    $('table tbody tr').eq(fila).remove();

    var numFilas=$('table tr:not(:first)').length;
    for(i=0;i<numFilas;i++){
        $('table tr:not(:first)').eq(i).find('.eliminaFila').attr("id", function(){
            var parts = this.id.match(/(\D+)(\d*)$/);
            return parts[1] + i;
        });
        $('table tr:not(:first)').eq(i).find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
                var parts = this.name.match(/(\D+)(\d*)$/);
                return parts[1] + i;
            }).attr("id", function(){
                return this.name;
            });
        }
  }
  else{
    alert('La tabla debe de contar con al menos 1 fila.');
  }
});