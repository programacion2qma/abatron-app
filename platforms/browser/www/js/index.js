/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        //app.receivedEvent('deviceready');
        definirRaiz();
        if(getLocal('session')==undefined){
            window.location.href='login.html';
        } else {
            document.removeEventListener( "backbutton", function(){}, false);
            document.addEventListener("backbutton", handleBackButton, false);
            localStorage.removeItem("codigoRegistro");
            $('.cerrarSesion').click(function(){
                localStorage.removeItem("session");
                window.location.href='login.html';
            });
            $(document).on('change','#codigoCliente',function(){
                obtenerInstalaciones();
            });
            $(document).on('change','#codigoDepartamento',function(){
                obtenerEquipo();
                obtenerDistancia();
            });
            $(document).on('change','#codigoTecnicoResponsable',function(){
                obtenerCodigoInternoTrabajo();
            });
            $(document).on('change','#numeroAviso',function(){
                obtenerEquipoDesdeAviso();
            });
            $(document).on('click','.itemNavigation',function(){
                cambiaVista($(this));
            });
            $(document).on('click','.eliminaContenedorImagen',function(){
                eliminarImagen($(this));
            });
            $("#botonMenu,.buttonLi").click(function() {
                $(".ulMenu").toggle("100", function() {
                
                });
            });
            /*$.ajax({
                type: 'get',
                url: 'views/clientes/index.html',
                data: null,
                success: function(data) {
                    $('#content').html(data);
                    var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerClientesApp();',{'codigoCliente':83});
                    consulta.done(function(respuesta){
                        $('table tbody ').html(respuesta);
                        navegacion();
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert("Error detectado: " + textStatus + "\nExcepcion: " + errorThrown);
                }
            });*/
        }
        /*var consulta=$.post('http://192.168.1.128:8888/abatron/gestionesAjaxApp.php?funcion=obtenerClientesApp();',{'codigoCliente':83});
        consulta.done(function(respuesta){
            $('table tbody ').html(respuesta);
            navegacion();
        });*/
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

function handleBackButton(){
    if($('#atrasOculto').length) {
        cambiaVista($('#atrasOculto'));
    } else {
        alert('Para volver a la pantalla de inicio de sesión, antes, cierre sesión');
    }
}


