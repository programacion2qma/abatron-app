/*function navegacion(){
	$(".itemNavigation").each(function( index ) {
    $(this).click(function(){
      cambiaVista($(this));
    });
  });
}*/
function definirRaiz(){
  var developer=0;
  if(developer==0){
    localStorage.setItem("raiz",'http://192.168.1.40:8888');
  } else {
    localStorage.setItem("raiz",'https://crmparapymes.com.es');
  }
}
$('#content').on("keydown",function(event){
       if (event.keyCode == 13) {
          if($('#tablaOtrasTareas').length || $('#tablaMateriales').length){
            event.preventDefault();
            insertaFila("tablaOtrasTareas");
            $('#tablaOtrasTareas tr:last .input-large').focus();
          }
       }
});
function cambiaVista(elem){
	if(elem.attr('view')=='index.html'){
		window.location.href='index.html';
	} else {
		var url='views/'+elem.attr('view');
	}
    if(elem.attr('codigo')!=undefined){
      localStorage.setItem("codigoRegistro",elem.attr('codigo'));
    } else{
      localStorage.removeItem("codigoRegistro");
    }
    if(elem.attr('numeroAviso')!=undefined){
      localStorage.setItem("numeroAviso",elem.attr('numeroAviso'));
    } else {
      localStorage.removeItem("numeroAviso");
    }
    if(elem.attr('foto')!=undefined){
      localStorage.setItem("imagenUnica",elem.attr('foto'));
    } else {
      localStorage.removeItem("imagenUnica");
    }
    $.ajax({
        type: 'get',
        url: url,
        data: null,
        success: function(data) {
            $('#content').html(data)
            if(codigoRegistro()!=''){
        		  campo='<input class="hide" id="codigo" name="codigo" value="'+codigoRegistro()+'">'
        		  $("form").append(campo);
    		    }
            var partes=url.split('/');
            setTimeout(function(){ 
              inicializaciones(partes[1],partes[2]);
              $('.contList').fadeIn(500);
            },150);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert("Error detectado: " + textStatus + "\nExcepcion: " + errorThrown);
        }
    });
}

function inicializaciones(tabla,vista){
	if(tabla=='avisos'){
		inicioAvisos(vista);
	} else if(tabla=='trabajos'){
    inicioTrabajos(vista);
  }
}

function inicioAvisos(vista){
	if(vista=='form.html'){
		if(codigoRegistro()!=''){
			var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerRegistroApp();'
        	,{'tabla':'avisos2','codigo':codigoRegistro()}
        	,function(respuesta){
            datosBD(respuesta);
            $('#codigoDepartamentoOculto').val(respuesta.codigoDepartamento);
            obtenerInstalaciones();
            var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=compruebaPerfilApp();',
            {'perfil':'Técnico','codigoUsuario':getLocal('codigoUsuario')});
            consulta.done(function(respuesta){
              if(respuesta=='NO'){
                var button="<button id='actualizar' tabla='avisos2' view='avisos/index.html'> Actualizar </button>";
                $('form').append(button);
              }
            });
            oyenteActualizar();
      },'json')
    } else {
        var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerCodigoInternoApp();',{tabla:'avisos2',campo:'codigoInterno',where:''});
          consulta.done(function(respuesta){
            var codigoInterno=respuesta;
            $('#codigoInterno').val(respuesta)
        });
        $('#eliminado').val('NO');
        fechaHoy('#fecha','web');
        obtieneHora('#hora')
        var button="<button id='insertar' tabla='avisos2' view='avisos/index.html'> Insertar </button>";
        $('form').append(button);
        oyenteInsertar();
    }
    //navegacion();
	} else if(vista=='gestion.html'){

	} else if(vista=='index.html'){
    var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=compruebaPerfilApp();',
      {'perfil':'Técnico','codigoUsuario':getLocal('codigoUsuario')});
    consulta.done(function(respuesta){
      if(respuesta=='SI'){
        $('#aniadirAviso').addClass('hide');
      }
    });
    var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerAvisosApp();',{});
    consulta.done(function(respuesta){
      $('table tbody').html(respuesta);
      filtroTabla();
    });
  }
}

function inicioTrabajos(vista){
  if(vista=='form.html'){
    if(codigoRegistro()!=''){
      var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerRegistroApp();'
          ,{'tabla':'trabajos','codigo':codigoRegistro()}
          ,function(respuesta){
            datosBD(respuesta);
            $('#codigoInternoMostrar').html(str_pad($('#codigoInterno').val(),3,'0'));
            $('#codigoDepartamentoOculto').val(respuesta.codigoDepartamento);
            $('#codigoEquipoOculto').val(respuesta.codigoEquipo);
            obtenerInstalaciones();
            var button="<button id='actualizar' class='actualizar' tabla='trabajos' view='trabajos/gestion.html' codigo='"+codigoRegistro()+"'> Actualizar </button>";
            $('form').append(button);
            $('#atrasFormTrabajos').removeClass('itemNavigation');
            $('#atrasFormTrabajos').addClass('actualizar');
            $('#atrasFormTrabajos').attr('tabla','trabajos');
            $('#atrasFormTrabajos').attr('codigo',codigoRegistro());
            $('#atrasOculto').attr('codigo',codigoRegistro());
            $('#atrasFormTrabajos').attr('view','trabajos/gestion.html');
            oyenteActualizar();
      },'json')
    } else {
        $('#codigoInterno').val(0);
        $('#codigoInternoMostrar').html('000');
        obtenerCodigoInternoTrabajo();
        $('#eliminado').val('NO');
        $('#equipoRetirado').val('NO');
        $('#revisado').val('NO');
        $('#enviado').val('NO');
        $('#tipo').val('MANTENIMIENTO');
        fechaHoy('#fechaInicio','web');
        fechaHoy('#fechaFin','web');
        obtieneHora('#horaInicioD');
        obtieneHora('#horaFinD');
        obtieneHora('#horaInicioM');
        obtieneHora('#horaFinM');
        var button="<button id='insertar' tabla='trabajos' view='trabajos/gestion.html'> Insertar </button>";
        $('form').append(button);
        oyenteInsertar();
        $('#atrasFormTrabajos').unbind();
        $('#atrasFormTrabajos').removeClass('itemNavigation');
        $('#atrasFormTrabajos').click(function(e){
          e.preventDefault();
          var r = confirm("¿Deseas salir sin guardar?");
          if (r==true) {
            cambiaVista($(this));
          }
        })
    }
  } else if(vista=='form1.html'){
    var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerTecnicosTrabajoApp();'
          ,{'tabla':'trabajos','codigo':codigoRegistro()}
          ,function(respuesta){
            var tabla=$('#tablaTecnicos');
            if(respuesta.length>0){
              for(var i=0;i<respuesta.length;i++){
                tabla.find('tbody').append('<tr></tr>');
                var tr=tabla.find('tbody').find('tr:last');
                campoSelectConsultaTablaTecnicos(tr,'codigoTecnico'+i
                ,'SELECT personal.codigo, UPPER(CONCAT(personal.nombre," ",apellidos)) AS texto FROM personal INNER JOIN puestosTrabajo ON personal.codigoPuestoTrabajo=puestosTrabajo.codigo WHERE puestosTrabajo.funcion = "Tecnico" OR puestosTrabajo.funcion = "Técnico" OR personal.codigo=1',respuesta[i].codigoTecnico);
                botonEliminaFila(tr,i);
              }
            } else {
              tabla.find('tbody').append('<tr></tr>');
              var tr=tabla.find('tbody').find('tr:last');
              campoSelectConsultaTablaTecnicos(tr,'codigoTecnico0'
              ,'SELECT personal.codigo, UPPER(CONCAT(personal.nombre," ",apellidos)) AS texto FROM personal INNER JOIN puestosTrabajo ON personal.codigoPuestoTrabajo=puestosTrabajo.codigo WHERE puestosTrabajo.funcion = "Tecnico" OR puestosTrabajo.funcion = "Técnico" OR personal.codigo=1');
              botonEliminaFila(tr,i);
            }
            var button="<button id='insertar' class='insertar hide' tabla='trabajos' view='trabajos/gestion.html' codigo='"+codigoRegistro()+"'> Actualizar </button>";
            $('form').append(button);
            $('#atrasFormTrabajos').removeClass('itemNavigation');
            $('#atrasFormTrabajos').addClass('insertar');
            $('#atrasFormTrabajos').attr('tabla','trabajos');
            $('#atrasFormTrabajos').attr('codigo',codigoRegistro());
            $('#atrasOculto').attr('codigo',codigoRegistro());
            $('#atrasFormTrabajos').attr('view','trabajos/gestion.html');
            oyenteInsertarTecnicos();
      },'json')
  } else if(vista=='form2.html'){
    var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerRegistroApp();'
          ,{'tabla':'trabajos','codigo':codigoRegistro()}
          ,function(respuesta){
            datosBD(respuesta);
            if(respuesta.operacion!=''){
              var operaciones=respuesta.operacion.split('&{}&');
              for(var i=0;i<operaciones.length;i++){
                $('input[value='+operaciones[i]+']').attr('checked','checked');
              }
            }
            oyenteChecksOperacion();
            $('input[name=equipoRetirado]').change(function(){
              oyenteOtrosCamposOperacion();
            });
            var button="<button id='actualizar' class='actualizar hide' tabla='trabajos' view='trabajos/gestion.html' codigo='"+codigoRegistro()+"'> Actualizar </button>";
            $('form').append(button);
            $('#atrasFormTrabajos').removeClass('itemNavigation');
            $('#atrasFormTrabajos').addClass('actualizar');
            $('#atrasFormTrabajos').attr('tabla','trabajos');
            $('#atrasFormTrabajos').attr('codigo',codigoRegistro());
            $('#atrasOculto').attr('codigo',codigoRegistro());
            $('#atrasOculto').attr('codigo',codigoRegistro());
            $('#atrasFormTrabajos').attr('view','trabajos/gestion.html');
            oyenteActualizarOperacion();
            $('.contentList').show(100);
      },'json');
    /*consulta.done(function(){
      
    });*/
  } else if(vista=='form3.html'){
    var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerTareasTrabajoApp();'
          ,{'tabla':'trabajos','codigo':codigoRegistro()}
          ,function(respuesta){
            var tabla=$('#tablaOtrasTareas');
            if(respuesta.texto.length>0){
              for(var j=0;j<respuesta.texto.length;j++){
                tabla.find('tbody').append('<tr></tr>');
                var tr=tabla.find('tbody').find('tr:last');
                botonEliminaFila(tr,i);
                campoTextoTabla(tr,'otraTarea'+j);
                $('#otraTarea'+j).val(respuesta.texto[j].valor);
              }
            } else {
              tabla.find('tbody').append('<tr></tr>');
              var tr=tabla.find('tbody').find('tr:last');
              botonEliminaFila(tr,0);
              campoTextoTabla(tr,'otraTarea0');
            }
            listado=['checkChequeo','checkSustBotella','checkSustElectrovalvula','checkSustBomba','checkSustMotor','checkSustOsmosis','checkSustKit','checkSustConjunto','checkSustResina','checkSustModulo','checkMedidas'];
            listado2=['Chequeo General del Equipo','Sustitución Bot. Acond. 20 Litros','Sustitución Electroválvula','Sustitución Bomba','Sustitución Motor','Sustitución Ómosis','Sustitución Kit Depuración','Sustitución Conjunto Filtr.','Sustitución Resina/Acond.','Sustitución Módulo Control','Medida Niveles y Presiones'];
            $('form').prepend('<div id="divCheckTareas"></div>');
            var div=$('#divCheckTareas');
            for(var i=0;i<listado.length;i++){
              campoCheckIndividual(div,listado2[i],'checkTareas'+i,listado[i])
            }
            for(var i=0;i<respuesta.check.length;i++){
              $('input[type=checkbox][value='+respuesta.check[i].valor+']').attr('checked','checked');
            }
            var button="<button id='insertar' class='insertar hide' tabla='trabajos' view='trabajos/gestion.html' codigo='"+codigoRegistro()+"'> Actualizar </button>";
            $('form').append(button);
            $('#atrasFormTrabajos').removeClass('itemNavigation');
            $('#atrasFormTrabajos').addClass('insertar');
            $('#atrasFormTrabajos').attr('tabla','trabajos');
            $('#atrasFormTrabajos').attr('codigo',codigoRegistro());
            $('#atrasOculto').attr('codigo',codigoRegistro());
            oyenteInsertarTareas();
      },'json')
  } else if(vista=='form4.html'){
    var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerNivelesYPrevionesApp();'
          ,{'codigo':codigoRegistro()}
          ,function(respuesta){
            datosBD(respuesta);
            campoOculto('codigoTrabajo');
            $('#codigoTrabajo').val(codigoRegistro());
            var button="<button id='actualizar' class='actualizar hide' tabla='trabajos_medidas' view='trabajos/gestion.html' codigo='"+codigoRegistro()+"'> Actualizar </button>";
            $('form').append(button)
            $('#atrasFormTrabajos').removeClass('itemNavigation');
            $('#atrasFormTrabajos').addClass('actualizar');
            $('#atrasFormTrabajos').attr('tabla','trabajos_medidas');
            $('#atrasFormTrabajos').attr('codigo',codigoRegistro());
            $('#atrasOculto').attr('codigo',codigoRegistro());
            oyenteActualizarMedidas(respuesta.codigo);
            if(respuesta.numeroOSM!='NULL' && respuesta.numeroOSM>2){
              $('table tr:nth-child(14),table tr:nth-child(15),table tr:nth-child(23),table tr:nth-child(24)').removeClass('hide');
            } else {
              $('table tr:nth-child(14),table tr:nth-child(15),table tr:nth-child(23),table tr:nth-child(24)').addClass('hide');
            }
      },'json')
  } else if(vista=='form5.html'){
    var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerMaterialesTrabajoApp();'
          ,{'tabla':'trabajos','codigo':codigoRegistro()}
          ,function(respuesta){
            var tabla=$('#tablaMateriales');
            if(respuesta.length>0){
              for(var i=0;i<respuesta.length;i++){
                tabla.find('tbody').append('<tr></tr>');
                var tr=tabla.find('tbody').find('tr:last');
                botonEliminaFila(tr,i);
                campoSelectConsultaTablaMateriales(tr,i
                  ,'SELECT codigo, CONCAT(codigoInterno," - ",nombre) AS texto FROM materiales ORDER BY nombre',respuesta[i].codigoMaterial,'selectpicker span2 show-tick selectRight',respuesta[i].cantidad);
              }
            } else {
                tabla.find('tbody').append('<tr></tr>');
                var tr=tabla.find('tbody').find('tr:last');
                botonEliminaFila(tr,0);
                campoSelectConsultaTablaMateriales(tr,0
                ,'SELECT codigo, CONCAT(codigoInterno," - ",nombre) AS texto FROM materiales ORDER BY codigoInterno',0,'selectpicker span2 show-tick selectRight');
            }
            var button="<button id='insertar' class='insertar hide' tabla='trabajos' view='trabajos/gestion.html' codigo='"+codigoRegistro()+"'> Actualizar </button>";
            $('form').append(button);
            $('#atrasFormTrabajos').removeClass('itemNavigation');
            $('#atrasFormTrabajos').addClass('insertar');
            $('#atrasFormTrabajos').attr('tabla','trabajos');
            $('#atrasFormTrabajos').attr('codigo',codigoRegistro());
            $('#atrasOculto').attr('codigo',codigoRegistro());
            oyenteInsertarMateriales();
      },'json')
  } else if(vista=='form6.html'){
    $('#tomarFoto').unbind();
    $('#tomarFoto').click(function(e){
      e.preventDefault();
      navigator.camera.getPicture(onSuccess, onFail, { quality: 50,
        destinationType: Camera.DestinationType.DATA_URL
      });    
    });
    var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerImagenesApp();'
          ,{'codigo':codigoRegistro()}
          ,function(respuesta){
          for(var i=0;i<respuesta.length;i++){
            var elemento=contenedorImagen(respuesta[i],i);
            if(i==0){
              $('form').prepend(elemento);
            } else {
              j=i-1;
              $('#contenedorImagen'+j).after(elemento);
            }
          }
          $(".photo").each(function( index ) {
            var href=$(this).find('img').attr('src');
            $(this).attr('href',href);
            $(this).fancyZoom();
          });
    },'json')
    var button="<br/><br/><button id='insertar' class='insertar hide' tabla='trabajos' view='trabajos/gestion.html' codigo='"+codigoRegistro()+"'> Actualizar </button>";
    $('form').append(button);
    $('#atrasFormTrabajos').removeClass('itemNavigation');
    $('#atrasFormTrabajos').addClass('insertar');
    $('#atrasFormTrabajos').attr('tabla','trabajos');
    $('#atrasFormTrabajos').attr('codigo',codigoRegistro());
    $('#nuevaCarga').attr('codigo',codigoRegistro());
    oyenteInsertarImagenes();
  } else if(vista=='form7.html'){
    var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerRegistroApp();'
          ,{'tabla':'trabajos','codigo':codigoRegistro()}
          ,function(respuesta){
            datosBD(respuesta);
            var button="<button id='actualizar' class='actualizar hide' tabla='trabajos' view='trabajos/gestion.html' codigo='"+codigoRegistro()+"'> Actualizar </button>";
            $('form').append(button);
            $('#atrasFormTrabajos').removeClass('itemNavigation');
            $('#atrasFormTrabajos').addClass('actualizar');
            $('#atrasFormTrabajos').attr('tabla','trabajos');
            $('#atrasFormTrabajos').attr('codigo',codigoRegistro());
            $('#atrasOculto').attr('codigo',codigoRegistro());
            $('#atrasFormTrabajos').attr('view','trabajos/gestion.html');
            oyenteActualizar();
      },'json')
  } else if(vista=='form8.html'){
    var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerRegistroApp();'
          ,{'tabla':'trabajos','codigo':codigoRegistro()}
          ,function(respuesta){
            datosBD(respuesta);
            $(".sigPad1").each(function( index ) {
              inicializaFirma($(this).find('input.hide').attr('id'));
            });
            var button="<button id='actualizar' class='actualizar hide' tabla='trabajos' view='trabajos/gestion.html' codigo='"+codigoRegistro()+"'> Actualizar </button>";
            $('form').append(button);
            $('#atrasFormTrabajos').removeClass('itemNavigation');
            $('#atrasFormTrabajos').addClass('actualizar');
            $('#atrasFormTrabajos').attr('tabla','trabajos');
            $('#atrasFormTrabajos').attr('codigo',codigoRegistro());
            $('#atrasOculto').attr('codigo',codigoRegistro());
            $('#atrasFormTrabajos').attr('view','trabajos/gestion.html');
            oyenteActualizar();
      },'json')
  } else if(vista=='gestion.html'){
    $('.shortcut').attr('codigo',codigoRegistro());
    var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerRegistroApp();'
          ,{'tabla':'trabajos','codigo':codigoRegistro()}
          ,function(respuesta){
            if(respuesta.finalizado=='SI'){
              $('#botonRevisado').removeClass('hide');
              $('#botonRevisado').click(function(){
                cambiarValorCampo('trabajos',codigoRegistro(),'revisado','SI');
                cambiaVista($('#botonRevisado'));
              });
            } else {
              $('#botonFinalizado').removeClass('hide');
              $('#botonFinalizado').click(function(){
                cambiarValorCampo('trabajos',codigoRegistro(),'finalizado','SI');
                cambiaVista($('#botonRevisado'));
              });
            }
            if(!respuesta.operacion.includes('REVISION') && !respuesta.operacion.includes('REPARACION')){
              $('#botonNiveles').removeClass('itemNavigation');
              $('#botonNiveles').unbind();
              $('#botonNiveles').click(function(e){
                e.preventDefault();
                alert('Para acceder a esta sección el tipo de operación debe ser \'Reparación\' o \'Revisión\'');
              });
            }
    },'json');
  } else if(vista=='index.html'){
    //pasarle el parametro del técnico
    var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerTrabajosApp();',
      {'codigoUsuario':getLocal('codigoUsuario'),'codigoTecnico':getLocal('codigoTecnico')});
    consulta.done(function(respuesta){
      $('table tbody').html(respuesta);
      filtroTabla();
      $('.contentList .itemNavigation').unbind();
      $('.contentList .itemNavigation').on('tap', function(e) { 
        e.preventDefault();
        cambiaVista($(this));
      });
      $('.contentList .itemNavigation').on('taphold', function(e) { 
        eliminaTrabajo($(this));
      });
      //navegacion();
    });
  } else if(vista=='foto.html'){
    $('#imagenUnica').attr('src',raiz()+'/abatron/documentos/incidencias/'+getLocal('imagenUnica'));
    $('#atrasFormTrabajos').attr('tabla','trabajos');
    $('#atrasFormTrabajos').attr('codigo',codigoRegistro());
    $('#atrasOculto').attr('codigo',codigoRegistro());
    $('#atrasFormTrabajos').attr('view','trabajos/form6.html');
  }
}

function eliminaTrabajo(elem){
  var res=confirm('¿Estas seguro que quieres eliminar este parte?');
  if(res){
    var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=eliminaDatosApp();',
      {'codigo':elem.attr('codigo'),'tabla':'trabajos'});
    consulta.done(function(respuesta){
      elem.parent().remove();
    });
    
  }
}

function datosBD(datos){
  for (var k in datos){
    var tipo=$('[name='+k+']').prop('nodeName');
    if(tipo=='INPUT'){
      if(k.includes('fecha')){
        fecha=datos[k].split('-');
        $('[name='+k+']').val(fecha[2]+'/'+fecha[1]+'/'+fecha[0]);
      } else if(k.includes('hora')){
        hora=datos[k].split(':');
        $('[name='+k+']').val(hora[0]+':'+hora[1]);
      } else if($('[name='+k+']').attr('type')=='radio'){
        $('[name='+k+'][value='+datos[k]+']').attr('checked','');
      } else {
        $('[name='+k+']').val(datos[k]);
      }
    } else if(tipo=='TEXTAREA'){
      $('[name='+k+']').html(datos[k]);
    } else {
      $('[name='+k+']').val(datos[k]).selectpicker('refresh');
    }
  }
}

function oyenteActualizar(){
	$('.actualizar').unbind();
   		$('.actualizar').click(function(e){
   			e.preventDefault();
   			var parametros = $('form').serializeArray();
    		parametros.push({name: 'tabla', value: $(this).attr('tabla')});
   			var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=actualizaDatosApp();',parametros);
        	consulta.done(function(respuesta){
            cambiaVista($('#actualizar'));
        });
   	});
}

function oyenteInsertar(){
	$('#insertar').unbind();
   		$('#insertar').click(function(e){
   			e.preventDefault();
   			var parametros = $(this).parent().serializeArray();
    		parametros.push({name: 'tabla', value: $(this).attr('tabla')});
   			var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=insertaDatosApp();',parametros);
        	consulta.done(function(respuesta){
            $('#insertar').attr('codigo',respuesta);
            cambiaVista($('#insertar'));
        });
   	});
}

function oyenteInsertarTecnicos(){
  $('.insertar').unbind();
      $('.insertar').click(function(e){
        e.preventDefault();
        var parametros = $('form').serializeArray();
        parametros.push({name: 'codigoTrabajo', value: codigoRegistro()});
        var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=insertaTecnicosTrabajoApp();',parametros);
          consulta.done(function(respuesta){
            cambiaVista($('#insertar'));
        });
    });
}

function oyenteInsertarMateriales(){
  $('.insertar').unbind();
      $('.insertar').click(function(e){
        e.preventDefault();
        var parametros = $('form').serializeArray();
        parametros.push({name: 'codigoTrabajo', value: codigoRegistro()});
        var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=insertaMaterialesTrabajoApp();',parametros);
          consulta.done(function(respuesta){
            cambiaVista($('#insertar'));
        });
    });
}

function oyenteInsertarTareas(){
  $('.insertar').unbind();
      $('.insertar').click(function(e){
        e.preventDefault();
        var parametros = $('form').serializeArray();
        parametros.push({name: 'codigoTrabajo', value: codigoRegistro()});
        var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=insertaTareasTrabajoApp();',parametros);
          consulta.done(function(respuesta){
            cambiaVista($('#insertar'));
        });
    });
}

function oyenteActualizarOperacion(){
  $('.actualizar').unbind();
      $('.actualizar').click(function(e){
        e.preventDefault();
        var parametros = $('form').serializeArray();
        parametros.push({name: 'tabla', value: $(this).attr('tabla')});
        var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=actualizaOperacionApp();',parametros);
          consulta.done(function(respuesta){
            cambiaVista($('#actualizar'));
        });
    });
}

function oyenteActualizarMedidas(codigo){
  $('.actualizar').unbind();
      $('.actualizar').click(function(e){
        e.preventDefault();
        var parametros = $('form').serializeArray();
        parametros.push({name: 'tabla', value: $(this).attr('tabla')});
        var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=actualizaMedidasApp();',parametros);
          consulta.done(function(respuesta){
            cambiaVista($('#actualizar'));
        });
    });
}

function raiz(){  
	return getLocal('raiz');
}

function codigoRegistro(){
	var res='';
	if(getLocal('codigoRegistro')!=undefined){
		res=getLocal('codigoRegistro');
	}
	return res;
}

function getLocal(key){
	return localStorage.getItem(key);
}

function campoTexto(texto,name,clase='input-large',readonly=''){
  var res="<div class='control-group'>";
  res+="<label class='control-label' for='"+name+"'>"+texto+":</label>";
  res+="<div class='controls'>";
	res+="<input name='"+name+"' id='"+name+"' class='"+clase+"' value='' "+readonly+">";
  res+="</div></div>";
  $('form').append(res);
}

function campoNumero(texto,name,clase='input-large',readonly=''){
  var res="<div class='control-group'>";
  res+="<label class='control-label' for='"+name+"'>"+texto+":</label>";
  res+="<div class='controls'>";
  res+="<input type='number' name='"+name+"' id='"+name+"' class='"+clase+"' value='' "+readonly+">";
  res+="</div></div>";
  $('form').append(res);
}

function campoTextoTabla(dentro,name,clase='input-large',readonly=''){
  var res="<td>";
  res+="<input name='"+name+"' id='"+name+"' class='"+clase+"' value='' "+readonly+">";
  res+="</td>";
  dentro.append(res);
}

function campoNumeroTabla(dentro,name,clase='input-large',readonly=''){
  var res="<td>";
  res+="<input type='number' name='"+name+"' id='"+name+"' class='"+clase+"' value='' "+readonly+">";
  res+="</td>";
  dentro.append(res);
}

function campoMedidas(texto,name,campos=2){
  var res="<tr><td>"+texto+"</td>";
  res+="<td>";
  clase1='input-mini';
  if(campos==1){
    clase1='hide';
  }
  res+="<input type='number' name='"+name+"Antiguo' id='"+name+"Antiguo' class='"+clase1+"' placeholder='Antigua'><br/>";
  res+="<input type='number' name='"+name+"' id='"+name+"' class='input-mini' placeholder='Nueva'>";
  res+="</td></tr>";
  $('table tbody').append(res);
}

function campoFecha(texto,name){
  campoTexto(texto,name,'input-small datepicker hasDatepicker','readonly');
  iniciaCamposFecha(name);
}

function campoSelect(texto,name,nombres,valores,clase='selectpicker span3 show-tick',busqueda="data-live-search='true'"){
  var res="<div class='control-group'>";
  res+="<label class='control-label' for='"+name+"'>"+texto+":</label>";
  res+="<div class='controls'>";
  res+="<select name='"+name+"' class='"+clase+"' id='"+name+"' "+busqueda+">";
  res+="<option value='NULL'></option>";
  for(var i=0;i<nombres.length;i++){
    if(valores[i]!=''){
      res+="<option value='"+valores[i]+"'>"+nombres[i]+"</option>";
    }
  }
  res+="</select></div></div>";
  $('form').append(res);
  iniciaCamposSelect(name);
}

function campoSelectConsulta(despues,texto,name,consulta,datos=0,clase='selectpicker span3 show-tick',busqueda="data-live-search='true'"){
 var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=selectConsultaApp();',{'consulta':consulta,'datos':datos});
        consulta.done(function(respuesta){
            var res="<div class='control-group'>";
            res+="<label class='control-label' for='"+name+"'>"+texto+":</label>";
            res+="<div class='controls'>";
            res+="<select name='"+name+"' class='"+clase+"' id='"+name+"' "+busqueda+">";
            res+="<option value='NULL'></option>";
            res+=respuesta;
            res+="</select></div></div>";
            if(despues==''){
              $('form').append(res);
            } else {
              despues.after(res);
            }
            iniciaCamposSelect(name);
        });

}

function campoSelectConsultaTabla(dentro,name,consulta,datos=0,clase='selectpicker span3 show-tick'){
 var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=selectConsultaApp();',{'consulta':consulta,'datos':datos});
        consulta.done(function(respuesta){
            var res="<td>";
            res+="<select name='"+name+"' class='"+clase+"' id='"+name+"'>";
            res+="<option value='NULL'></option>";
            res+=respuesta;
            res+="</select></td>";
            dentro.append(res);
            iniciaCamposSelect(name);
        });

}

function campoSelectConsultaTablaMateriales(dentro,i,consulta,datos=0,clase='selectpicker span3 show-tick',cantidad=0){
 var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=selectConsultaApp();',{'consulta':consulta,'datos':datos});
        consulta.done(function(respuesta){
            name='codigoMaterial'+i;
            var res="<td>";
            res+="<select name='"+name+"' class='"+clase+"' id='"+name+"' data-live-search='true'>";
            res+="<option value='NULL'></option>";
            res+=respuesta;
            res+="</select></td>";
            dentro.append(res);
            iniciaCamposSelect(name);
            campoNumeroTabla(dentro,'cantidad'+i,'input-mini');
            $('#cantidad'+i).val(cantidad);
        });

}

function campoSelectConsultaTablaTecnicos(dentro,name,consulta,datos=0,clase='selectpicker span3 show-tick'){
 var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=selectConsultaApp();',{'consulta':consulta,'datos':datos});
        consulta.done(function(respuesta){
            var res="<td>";
            res+="<select name='"+name+"' class='"+clase+"' id='"+name+"'>";
            res+="<option value='NULL'></option>";
            res+=respuesta;
            res+="</select></td>";
            dentro.append(res);
            iniciaCamposSelect(name);
            var i=name.match(/(\D+)(\d*)$/);
            i=i[2];
            $('#codigoTecnico'+i).change(function(){
              oyenteTecnicoRepetido(i);
            });
        });

}

function campoSelectAviso(despues,texto,name,clase='selectpicker span1 show-tick'){
  if(codigoRegistro()!=''){
    var codigo=codigoRegistro();
  } else {
    var codigo=0;
  }
  var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=selectAvisoApp();',{'codigo':codigo});
        consulta.done(function(respuesta){
            var res="<div class='control-group'>";
            res+="<label class='control-label' for='"+name+"'>"+texto+":</label>";
            res+="<div class='controls'>";
            res+="<select name='"+name+"' class='"+clase+"' id='"+name+"'>";
            res+="<option value='NULL'></option>";
            res+=respuesta;
            res+="</select></div></div>";
            if(despues==''){
              $('form').append(res);
            } else {
              despues.after(res);
            }
            iniciaCamposSelect(name);
            if(getLocal('numeroAviso')!=undefined){
              $('#numeroAviso').val(getLocal('numeroAviso')).selectpicker('refresh');
              obtenerEquipoDesdeAviso();
            }

        });

}

function campoRadio(dentro,texto,name,valorPorDefecto='NO',textosCampos=['Si','No'],valoresCampos=['SI','NO'],salto='NO'){
  var res="<div class='control-group'>";
  res+="<label class='control-label' for='"+name+"'>"+texto+":</label>";
  res+="<div class='controls nowrap'>";
  for(var i=0;i<textosCampos.length;i++){
    res+="<label class='radio inline'>";
    res+="<input type='radio' name='"+name+"' value='"+valoresCampos[i]+"'";
    if(valoresCampos[i]==valorPorDefecto){
      res+=" checked='checked'";
    }
    res+=">"+textosCampos[i]+"</label>";
    if(salto=='SI'){
      res+="<br/>";
    }
  }
  res+="</div></div>";
  if(dentro==''){
    $('form').append(res);
  } else {
    dentro.append(res);
  }
}

function campoOculto(name){
  var res="<input type='hidden' name='"+name+"' id='"+name+"'>";
  $('form').append(res);
}

function campoDato(texto,id){
  var res="<div class='control-group'>"; 
  res+="<label class='control-label'>"+texto+":</label>";
  res+="<div class='controls datoSinInput' id='"+id+"'>";
  res+="</div></div>";
  $('form').append(res);
}

function areaTexto(texto,name,clase='areaTexto',readonly=''){
  var res="<div class='control-group'>";
  res+="<label class='control-label' for='"+name+"'>"+texto+":</label>";
  res+="<div class='controls'>";
  res+="<textarea name='"+name+"' id='"+name+"' class='"+clase+"' "+readonly+"></textarea>";
  res+="</div></div>";
  $('form').append(res);
}

function campoRangoHora(texto,name,name2){
  var res="<div class='control-group'>";
  res+="<label class='control-label' for='"+name+"'>"+texto+":</label>";
  res+="<div class='controls'>";
  res+="<input name='"+name+"' id='"+name+"' class='input-mini' value=''>";
  res+=" A: <input name='"+name2+"' id='"+name2+"' class='input-mini' value=''>";
  res+="</div></div>";
  $('form').append(res);
}

function campoFirma(texto,name){
  var res="<div class='control-group sigPad1 "+name+"'>";
  res+="<label class='control-label' for='"+name+"'>"+texto+":</label>";
  res+="<div class='controls'>";
  res+="<canvas class='pad' width='300' height='150'></canvas>";
  res+="<div class='clearButton'><a href='#clear' class='btn btn-danger noAjax'><i class='icon-trash'></i> Borrar</a></div>";
  res+="<input class='hide' name='"+name+"' id='"+name+"'>";
  res+="</div></div>";
  $('form').append(res);
}

function campoCheck(dentro,texto,name,textosCampos=['Si','No'],valoresCampos=['SI','NO'],salto='NO'){
  var res="<div class='control-group'>";
  res+="<label class='control-label' for='"+name+"'>"+texto+":</label>";
  res+="<div class='controls nowrap'>";
  for(var i=0;i<textosCampos.length;i++){
    res+="<label class='checkbox inline'>";
    res+="<input type='checkbox' name='"+name+i+"' value='"+valoresCampos[i]+"'";
    res+=">"+textosCampos[i]+"</label>";
    if(salto=='SI'){
      res+="<br/>";
    }
  }
  if(dentro==''){
    $('form').append(res);
  } else {
    dentro.append(res);
  }
}

function campoCheckIndividual(dentro,texto,name,valorCampo='SI'){
  var res="<label class='checkbox inline'>";
  res+="<input type='checkbox' name='"+name+"' id='"+name+"' value='"+valorCampo+"'>";
  res+=texto+"</label><br/>";
  if(dentro==''){
    $('form').append(res);
  } else {
    dentro.append(res);
  }
}

function filtroTabla(){
  $('.datatable').dataTable().fnDestroy();
  $('.datatable').dataTable({
    "sDom": "<'row-fluid arriba'<'span6'l><'span6'f>r>t<'row-fluid abajo'<'span6'i><'span6'p>>",
    "sPaginationType": "bootstrap",
    "stateSave":true,
    stateSaveCallback:function(settings, data){//MODIFICACIÓN 22/09/2015: uso del callback personalizado para que funcione el stateSave
        var idTabla=window.location.pathname.replace(/[- ._index.php/]*/g,'');//Uso como ID de la tabla la ruta de la página donde se encuentra (quitándole el index.php en caso de que existiera, para que no haya diferencias entre el menú y los botones de Volver)
        if(compruebaAlmacenamientoHTML5()) {
            window.localStorage.setItem(idTabla,JSON.stringify(data));//Guarda el estado de la tabla en el almacenamiento local
        }
    },
    stateLoadCallback: function(settings){//MODIFICACIÓN 22/09/2015: uso del callback personalizado para que funcione el stateSave
        var idTabla=window.location.pathname.replace(/[- ._index.php/]*/g,'');
        var res=false
        if(compruebaAlmacenamientoHTML5()) {
            res=JSON.parse(window.localStorage.getItem(idTabla));
        }
        return res;
    },
   "length":false, 
  "aLengthMenu":[ 10, 25, 100, 500, 1000 ],
    "iDisplayLength":25,
    "oLanguage": {
      "sLengthMenu": "_MENU_ registros por página",
      "sSearch":"Búsqueda:",
      "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente","sLast":"Último","sFirst":"Primero"},
      "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
      "sEmptyTable":"Aún no hay datos que mostrar",
      "sInfoEmpty":"",
      'sInfoFiltered':"(Filtrado de un total de _MAX_ registros)",
      'sZeroRecords':'No se han encontrado coincidencias'
    }
});

$('.dataTables_wrapper').find('.dataTables_paginate a').each(function(){
  $(this).addClass('noAjax');
});
}

function compruebaAlmacenamientoHTML5(){//MODIFICACIÓN 22/09/2015: nueva función
    try{
        return 'localStorage' in window && window['localStorage'] !== null;
    }
    catch (e){
        return false;
    }
}

function iniciaCamposFecha(id){
  $('#'+id).datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
}

function iniciaCamposSelect(id){
  $('#'+id).selectpicker();
}

function fechaHoy(campo='',tipo='bd'){
  var hoy = new Date();
  var dd = hoy.getDate();
  if(dd<10){
    dd='0'+dd;
  }
  var mm = hoy.getMonth()+1;
  if(mm<10){
    mm='0'+mm;
  }
  var yyyy = hoy.getFullYear();
  if(tipo=='bd'){
    var res=yyyy+'-'+mm+'-'+dd;
  } else {
    var res=dd+'/'+mm+'/'+yyyy;
  }
  if(campo!=''){
    $(campo).val(res)
  } else {
    return res;
  }
}

function obtieneHora(campo=''){
  var ahora = new Date();
  var hh=ahora.getHours();
  if(hh<10){
    hh='0'+hh;
  }
  var mm=ahora.getMinutes();
  if(mm<10){
    mm='0'+mm;
  }
  var res = cad=hh+":"+mm+":00"; 
  if(campo!=''){
    $(campo).val(res)
  } else {
    return res;
  }
}

function obtenerInstalaciones(){
  if($('#codigoCliente option:selected').val()!='NULL'){
    var codigoCliente=$('#codigoCliente option:selected').val();
    if($('#codigoDepartamentoOculto').length){
      var codigoDepartamento=$('#codigoDepartamentoOculto').val();
    } else {
      var codigoDepartamento=0;
    }
    var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerDepartamentosApp();',{'codigoCliente':codigoCliente,'codigoDepartamento':codigoDepartamento});
    consulta.done(function(respuesta){
      $('#codigoDepartamento').html(respuesta).selectpicker('refresh');
      obtenerEquipo();
    });
  } else {
    $('#codigoDepartamento').html('<option value="NULL"></option>').selectpicker('refresh');
  }
}

function obtenerDistancia(){
  if($('#codigoDepartamento option:selected').val()!='NULL'){
    var codigoDepartamento=$('#codigoDepartamento option:selected').val();
    var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerDistanciaApp();',{'codigoDepartamento':codigoDepartamento});
    consulta.done(function(respuesta){
      $('#km').val(respuesta);
    });
  } else {
    $('#km').val('');
  }
}

function obtenerEquipo(){
  var codigoCliente=$('#codigoCliente option:selected').val();
  var codigoDepartamento=$('#codigoDepartamento option:selected').val();
  if($('#codigoEquipoOculto').length){
    var codigoEquipo=$('#codigoEquipoOculto').val();
  } else {
    var codigoEquipo=0;
  }
  var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerEquipoApp();',{'codigoCliente':codigoCliente,'codigoDepartamento':codigoDepartamento,'codigoEquipo':codigoEquipo});
  consulta.done(function(respuesta){
    $('#codigoEquipo').html(respuesta).selectpicker('refresh');
  });
}

function obtenerCodigoInternoTrabajo(){
  if($('#codigoTecnicoResponsable option:selected').val()!='NULL'){
    var codigoTecnico=$('#codigoTecnicoResponsable option:selected').val();
    var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerCodigoInternoTrabajoApp();',
      {'codigoTecnico':codigoTecnico},
      function(respuesta){
        $('#codigoInterno').val(respuesta.codigoInterno);
        $('#codigoInternoMostrar').html(respuesta.codigoInternoMostrar);
    },'json');
  } else {
    $('#codigoInterno').val(0);
    $('#codigoInternoMostrar').html('000');
  }
}

function obtenerEquipoDesdeAviso(){
  if($('#numeroAviso option:selected').val()!='NULL' && $('#codigo').length==0){
    var codigoAviso=$('#numeroAviso option:selected').val();
    var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=obtenerEquipoDesdeAvisoApp();'
      ,{'codigoAviso':codigoAviso},
      function(respuesta){
      $('#codigoCliente').val(respuesta.codigoCliente).selectpicker('refresh');
      $('#codigoDepartamentoOculto').val(respuesta.codigoDepartamento);
      $('#codigoEquipo').val(respuesta.codigo).selectpicker('refresh');
      obtenerInstalaciones();
    },'json');
  }
}

function str_pad(valor,tamanio,caracter,lado='izq'){
  var res='';
  for(i=valor.length;i<tamanio;i++){
    res+=caracter;
  }
  if(lado=='izq'){
    res=res+valor;
  } else {
    res=valor+res;
  }
  return res;
}

function cambiarValorCampo(tabla,codigo,campo,valor){
  var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=cambiarValorCampoApp();',
    {'tabla':tabla,'codigo':codigo,'campo':campo,'valor':valor});
    consulta.done(function(respuesta){

    });
}

function inicializaFirma(selector){
  if($('#'+selector).length>0){
    var firma=$('#'+selector).val().trim();
    if(firma=='' || firma=='output'){
      $('.'+selector).signaturePad({drawOnly:true,lineTop:'130',penColour:'000000',validateFields:false,output:'#'+selector});
    } else{
      $('.'+selector).signaturePad({drawOnly:true,lineTop:'130',penColour:'000000',validateFields:false,output:'#'+selector}).regenerate(firma);
    }
  }       
}

function oyenteInsertarImagenes(){
  $('.insertar').unbind();
      $('.insertar').click(function(e){
        e.preventDefault();
        var parametros = $('form').serializeArray();
        parametros.push({name: 'codigoTrabajo', value: codigoRegistro()});
        var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=insertaImagenesTrabajosApp();',parametros);
          consulta.done(function(respuesta){
            cambiaVista($('#insertar'));
        });
    });
}
     
function onSuccess(imageData) {
  //var image = $('#imagen0');
  var image = "data:image/jpeg;base64," + imageData;
  var i=0;
  parametros=new Array();
  parametros.push({name: 'codigo', value: codigoRegistro()});
  parametros.push({name: 'imagen', value: image});
  var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=insertaFotoApp();',parametros);
      consulta.done(function(respuesta){
        //setTimeout(function(){ 
          cambiaVista($('#nuevaCarga'));
        //},550);
  });
  /*$(".contenedorImagen").each(function( index ) {
    i++;
  });
  var name='base64Imagen'+i;
  var res='<div id="contenedorImagen'+i+'" class="contenedorImagen">';
  res+='<a class="photo"><img src="'+image+'" /></a>';
  res+='<i id="eliminaContenedorImagen'+i+'" class="fa fa-trash-o eliminaContenedorImagen"></i>';
  res+="<input type='hidden' name='"+name+"' id='"+name+"' value='"+image+"'>";
  res+='</div>';
  if(i==0){
    $('form').prepend(res);
  } else {
    i=i-1;
    $('#contenedorImagen'+i).after(res);
  }
  $(".photo:last").each(function( index ) {
    var href=$(this).find('img').attr('src');
    $(this).attr('href',href);
    $(this).fancyZoom();
  });*/
}

function onFail(message) {
  if(message!='No Image Selected'){
    //alert('Failed because: ' + message);
  }
}

function contenedorImagen(datos,i){
  var name='imagenExiste'+i;
  var res='<div id="contenedorImagen'+i+'" class="contenedorImagen">';
  res+='<a class="photo"><img src="'+raiz()+'/abatron/documentos/incidencias/'+datos.ficheroImagen+'" /></a>';
  res+='<i id="eliminaContenedorImagen'+i+'" class="fa fa-trash-o eliminaContenedorImagen"></i>';
  res+="<input type='hidden' name='"+name+"' id='"+name+"' value='"+datos.codigo+"'>";
  res+='</div>';
  return res;
}

function eliminarImagen(elem){
  var i=elem.attr('id').match(/(\D+)(\d*)$/);
  var codigoImagen=$('#imagenExiste'+i[2]).val();
  $('#contenedorImagen'+i[2]).remove();
  var consulta=$.post(raiz()+'/abatron/gestionesAjaxApp.php?funcion=eliminaFotoApp();',{'codigoImagen':codigoImagen});
      consulta.done(function(respuesta){
  });
}

function botonEliminaFila(dentro,i){
  var res='<td style="text-align:center"><i name="eliminaFila'+i+'" id="eliminaFila'+i+'" class="fa fa-trash-o eliminaFila"></i></td>';
  dentro.append(res);
}

function oyenteChecksOperacion(){
  $("input[type=checkbox]").each(function( index ) {
    oyenteOtrosCamposOperacion();
    $(this).change(function(){
      var res='';
      for(var i=0;i<5;i++){
        if($('input[name=operacion'+i+']').attr('checked')=='checked'){
          if(res==''){
            res=$('input[name=operacion'+i+']').val();
          } else {
            res+='&{}&'+$('input[name=operacion'+i+']').val();
          }
        }
      }
      oyenteOtrosCamposOperacion();
      $('#operacion').val(res);
    });
  });
}

function oyenteOtrosCamposOperacion(){
  if($('input[name=operacion4]').attr('checked')=='checked'){
    $('#divOtraOperacion').removeClass('hide');
  } else {
    $('#divOtraOperacion').addClass('hide');
  }
  if($('input[name=operacion1]').attr('checked')=='checked'){
    $('#divReparacion').removeClass('hide');
  } else {
    $('#divReparacion').addClass('hide');
  }
  if($('input[name=equipoRetirado][value=SI]').attr('checked')){
    $('#divNuevoEquipo').removeClass('hide');
  } else {
    $('#divNuevoEquipo').addClass('hide');
  }
}

function divOtraOperacion(texto,name,clase='input-large',readonly=''){
  var res="<div id='divOtraOperacion'>";
  res+="<div class='control-group'>";
  res+="<label class='control-label' for='"+name+"'>"+texto+":</label>";
  res+="<div class='controls'>";
  res+="<input name='"+name+"' id='"+name+"' class='"+clase+"' value='' "+readonly+">";
  res+="</div></div></div>";
  $('form').append(res);
}

function oyenteTecnicoRepetido(i){
  var valor=$('#codigoTecnico'+i+' option:selected').val();
  var j=0;
  while($('#codigoTecnico'+j).length>0){
    if(i!=j){
      var valor2=$('#codigoTecnico'+j+' option:selected').val();
      if(valor2!='NULL' && valor==valor2){
        alert('Este técnico ya ha sido seleccionado');
        $('#codigoTecnico'+i).val('NULL').selectpicker('refresh');
      }
    }
    j++;
  }
}